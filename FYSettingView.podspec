Pod::Spec.new do |s|
  s.name             = 'FYSettingView'
  s.version          = '1.2'
  s.summary          = 'Cocoapods Test'
  s.description      = 'Cocoapods Test JJJJJ'
  s.homepage         = 'https://git.oschina.net/frontnetwork66/fysettingview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Felix Yin' => 'lafenglafenghaha@163.com' }
  s.source           = { :git => 'https://git.oschina.net/frontnetwork66/fysettingview.git', :tag => s.version}
  s.requires_arc = true
  s.ios.deployment_target = '8.0'

  s.source_files = 'FYSettingView/**/*.{h,m}'
end